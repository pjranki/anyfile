import os
import sys
import subprocess

from .abstract_file import AbstractFile
from .tmp_directory import TmpDirectory


class GZipFile(AbstractFile):

    @classmethod
    def match0(self, start):
        return start.startswith(b'\x1f\x8b\x08\x00')

    def namelist(self):
        name = os.path.basename(self.path)
        if name.endswith('.gz'):
            name = name[:-3]
        if name.endswith('.gzip'):
            name = name[:-5]
        return [name]

    def extract(self, member, path=None, pwd=None):
        if member not in self.namelist():
            raise FileNotFoundError(member)
        src = os.path.realpath(self.path)
        if not path:
            dst = os.path.realpath(os.path.join(os.getcwd(), os.path.basename(member.replace('/', os.sep))))
        dst = os.path.realpath(path)
        dst_dir = os.path.dirname(dst)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        if os.path.exists(dst):
            os.remove(dst)
        assert os.path.exists(src)
        assert os.path.exists(dst_dir)
        assert not os.path.exists(dst)
        cmdline = 'gzip -dc < {} > {}'.format(src, dst)
        p = subprocess.Popen(cmdline, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if 0 != p.returncode and os.path.exists(dst):
            if stderr.decode().lower().find('decompression OK'.lower()) != -1:
                if stderr.decode().lower().find('trailing garbage ignored'.lower()) != -1:
                    p.returncode = 0
        if 0 != p.returncode:
            sys.stdout.write(stdout.decode())
            sys.stderr.write(stderr.decode())
            raise Exception(cmdline)
        assert os.path.exists(dst)
