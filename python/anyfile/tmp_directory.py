import os
import sys
import tempfile
import contextlib
import subprocess

from .abstract_file import AbstractFile

class TmpDirectory(AbstractFile):

    def __init__(self, path=None, *args, **kwargs):
        super().__init__(path, *args, **kwargs)
        self.temp_dir = None

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        try:
            self.temp_dir = tempfile.TemporaryDirectory()
            return self
        except:
            super().__exit__(None, None, None)
            raise

    def __exit__(self, *args, **kwargs):
        try:
            self.temp_dir.cleanup()
            self.temp_dir = None
            return
        finally:
            super().__exit__(*args, **kwargs)

    @property
    def tmp(self):
        if not self.temp_dir:
            return None
        return os.path.realpath(self.temp_dir.name)

    def path_to_member(self, path):
        assert path.startswith(self.tmp + os.sep)
        member = path[len(self.tmp) + len(os.sep):].replace(os.sep, '/')
        return member

    def member_to_path(self, member):
        path = os.path.join(self.tmp, member.replace('/', os.sep))
        assert path.startswith(self.tmp)
        return path

    def namelist(self):
        name_list = list()
        for root, folders, files in os.walk(self.tmp):
            for folder in folders:
                path = os.path.join(root, folder)
                assert os.path.exists(path)
                name_list.append(self.path_to_member(path))
            for file_ in files:
                path = os.path.join(root, file_)
                if not os.path.islink(path):
                    assert os.path.exists(path)
                name_list.append(self.path_to_member(path))
        return name_list

    def extract(self, member, path=None, pwd=None):
        src = self.member_to_path(member)
        if not os.path.islink(src):
            if not os.path.exists(src):
                raise FileNotFoundError(member)
        if not path:
            dst = os.path.realpath(os.path.join(os.getcwd(), os.path.basename(member.replace('/', os.sep))))
        dst = os.path.abspath(path)
        dst_dir = os.path.dirname(dst)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        if os.path.isdir(src):
            if os.path.exists(dst):
                assert os.path.isdir(dst)
            else:
                os.makedirs(dst)
        elif os.path.isfile(src):
            if os.path.exists(dst):
                os.remove(dst)
            assert not os.path.exists(dst)
            args = ['cp', src, dst]
            p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            if 0 != p.returncode:
                sys.stdout.write(stdout.decode())
                sys.stderr.write(stderr.decode())
                raise Exception(' '.join(args))
        elif os.path.islink(src):
            if os.path.exists(dst):
                if os.path.islink(dst):
                    os.unlink(dst)
                if os.path.isfile(dst):
                    os.remove(dst)
                if os.path.isdir(dst):
                    os.rmdir(dst)
                assert not os.path.exists(dst)
            args = ['cp', '-P', src, dst]
            p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            if 0 != p.returncode:
                sys.stdout.write(stdout.decode())
                sys.stderr.write(stderr.decode())
                raise Exception(' '.join(args))
        if not os.path.islink(dst):
            assert os.path.exists(dst)

    @contextlib.contextmanager
    def openpath(self, member, mode='r'):
        path = self.member_to_path(member)
        if not os.path.islink(path):
            if not os.path.exists(path):
                raise FileNotFoundError(member)
        yield path
