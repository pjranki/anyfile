import os
import sys
import struct

from .named_location_file import NamedLocationFile

class QCBootImage(NamedLocationFile):
    HEADER_SIZE = 20

    @classmethod
    def match0(self, start):
        return start.startswith(b'UNCOMPRESSED_IMG')

    @classmethod
    def skip0(self, member):
        return member == 'header'  # stop infinite recursion on UNCOMPRESSED_IMG header

    def __init__(self, path, mode='r'):
        self.path = path
        self.dtb_size_list = list()

    def __enter__(self, *args, **kwargs):
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
            self.kernel_size = struct.unpack('<I', data[0x10: 0x14])[0]
            file_size = os.stat(self.path).st_size
            self.dtb_array_size = file_size - self.dtb_array_offset
            dtb_offset = self.dtb_array_offset
            while dtb_offset < file_size:
                handle.seek(dtb_offset, 0)
                data = handle.read(8)
                dtb_magic, dtb_size = struct.unpack('>II', data)
                if dtb_magic != 0xd00dfeed:
                    break
                self.dtb_size_list.append(dtb_size)
                dtb_offset += dtb_size
        assert self.dtb_array_size >= 0
        return self

    def __exit__(self, *args, **kwargs):
        pass

    @property
    def header_offset(self):
        return 0

    @property
    def header_size(self):
        return self.HEADER_SIZE

    @property
    def kernel_offset(self):
        return self.header_size

    @property
    def dtb_array_offset(self):
        return self.kernel_offset + self.kernel_size

    def named_locations(self):
        file_list = list()
        file_list.append(('header', self.header_offset, self.header_size))
        if self.kernel_size != 0:
            file_list.append(('kernel', self.kernel_offset, self.kernel_size))
        if self.dtb_array_size != 0:
            index = 0
            dtb_offset = self.dtb_array_offset
            for dtb_size in self.dtb_size_list:
                file_list.append(('dtb{}'.format(index), dtb_offset, dtb_size))
                dtb_offset += dtb_size
                index += 1
        return file_list
