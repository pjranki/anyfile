import os
import sys
import logging
import collections
import anyfile

from .tmp_directory import TmpDirectory

log = logging.getLogger()


CLASSES = list()
for name in dir(anyfile):
    if name.startswith('_'):
        continue
    class_ = getattr(anyfile, name)
    if not repr(class_).startswith('<class '):
        continue
    found = False
    for match_name in ['match0', 'match1', 'match2']:
        if match_name in dir(class_):
            found = True
            break
    if not found:
        continue
    CLASSES.append(class_)


class AnyFile(TmpDirectory):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.file = None
        self.sub_files = None

    def __enter__(self, *args, **kwargs):
        log.debug(os.path.basename(self.path))
        super().__enter__(*args, **kwargs)
        try:
            end = None
            with open(self.path, 'rb') as handle:
                start = handle.read(0x1000)
                try:
                    handle.seek(-0x1000, 2)
                    end = handle.read(0x1000)
                except OSError:
                    end = None
            start = start + (b'\x00' * (0x1000 - len(start)))
            if not end:
                end = start
            assert len(start)
            file_class_ = None
            for class_ in CLASSES:
                if getattr(class_, 'match0', None) and class_.match0(start):
                    file_class_ = class_
                    break
                if getattr(class_, 'match1', None) and class_.match1(start, end):
                    file_class_ = class_
                    break
                if getattr(class_, 'match2', None) and class_.match2(self.path):
                    file_class_ = class_
                    break
            if file_class_:
                self.sub_files = collections.OrderedDict()
                self.file = class_(self.path, self.mode)
                self.file.__enter__(*args, **kwargs)
                try:
                    for name in self.file.namelist():
                        if getattr(self.file, 'skip0', None):
                            if self.file.skip0(name):
                                continue
                        out = os.path.join(self.tmp, name)
                        self.file.extract(name, out)
                        if not os.path.isfile(out):
                            continue
                        assert os.path.exists(out)
                        sub_file = AnyFile(out, self.mode)
                        sub_file.__enter__(*args, **kwargs)
                        try:
                            self.sub_files[name] = sub_file
                        except:
                            sub_file.__exit__(None, None, None)
                            raise
                except:
                    for sub_file in self.sub_files.values():
                        try:
                            sub_file.__exit__(None, None, None)
                        except:
                            pass
                    self.file.__exit__(None, None, None)
                    self.file = None
                    self.sub_files = None
                    raise
        except:
            super().__exit__(None, None, None)
            raise
        return self

    def __exit__(self, *args, **kwargs):
        try:
            if self.file:
                try:
                    for sub_file in self.sub_files.values():
                        try:
                            sub_file.__exit__(None, None, None)
                        except:
                            pass
                finally:
                    self.file.__exit__(*args, **kwargs)
                    self.file = None
                    self.sub_files = None
        finally:
            super().__exit__(*args, **kwargs)
        return

    def namelist(self):
        name_list = list()
        if self.file:
            for name in self.file.namelist():
                name_list.append(name)
                sub_file = self.sub_files.get(name, None)
                if sub_file:
                    name_list.extend(['{}/{}'.format(name, sub_name) for sub_name in sub_file.namelist()])
        return name_list

    def extract(self, member, path=None, pwd=None):
        if not path:
            path = os.path.join(os.getcwd(), os.path.basename(member.replace('/', os.sep)))
        if self.file:
            for name in self.file.namelist():
                if name == member:
                    self.file.extract(name, path)
                    return
            for sub_name, sub_file in self.sub_files.items():
                prefix = '{}/'.format(sub_name)
                if member.startswith(prefix):
                    sub_file.extract(member[len(prefix):], path)
                    return
        raise FileNotFoundError(member)

    def extractall(self, path=None, members=None, pwd=None):
        if not path:
            path = os.getcwd()
        if not self.file:
            if members is not None and len(members) > 0:
                raise FileNotFoundError(members[0])
            return
        if not members:
            for member in self.file.namelist():
                dst = os.path.join(path, member.replace('/', os.sep))
                self.extract(member, dst)
                if not os.path.islink(dst):
                    assert os.path.exists(dst)
            for sub_name, sub_file in self.sub_files.items():
                prefix = '{}/'.format(sub_name)
                rel_folder_path = prefix[:-1].replace('/', os.sep)
                if rel_folder_path.find(os.sep) != -1:
                    rel_folder_path = '{}{}_{}.extracted'.format(os.path.dirname(rel_folder_path), os.sep, os.path.basename(rel_folder_path))
                else:
                    rel_folder_path = '_{}.extracted'.format(rel_folder_path)
                dst = os.path.join(path, rel_folder_path)
                sub_file.extractall(dst)
            return
        path = os.path.realpath(path)
        for member in members:
            if member in self.file.namelist():
                dst = os.path.join(path, member.replace('/', os.sep))
                self.extract(member, dst)
                if not os.path.islink(dst):
                    assert os.path.exists(dst)
            else:
                dst = None
                for sub_name, sub_file in self.sub_files.items():
                    prefix = '{}/'.format(sub_name)
                    if member.startswith(prefix):
                        sub_member = member[len(prefix):]
                        rel_folder_path = prefix[:-1].replace('/', os.sep)
                        rel_file_path = sub_member.replace('/', os.sep)
                        if rel_folder_path.find(os.sep) != -1:
                            rel_folder_path = '{}{}_{}.extracted'.format(os.path.dirname(rel_folder_path), os.sep, os.path.basename(rel_folder_path))
                        else:
                            rel_folder_path = '_{}.extracted'.format(rel_folder_path)
                        dst = os.path.join(path, rel_folder_path)
                        sub_file.extractall(dst, [sub_member])
                        break
                if not dst:
                    raise FileNotFoundError(member)
