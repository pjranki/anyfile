import os
import sys


class AbstractFile(object):

    def __init__(self, path, mode='r'):
        self.path = path
        self.mode = mode

    def __enter__(self, *args, **kwargs):
        return self

    def __exit__(self, *args, **kwargs):
        return

    def namelist(self):
        return list()

    def extract(self, member, path=None, pwd=None):
        raise FileNotFoundError(member)

    def extractall(self, path=None, members=None, pwd=None):
        if not path:
            path = os.getcwd()
        if not members:
            members = self.namelist()
        path = os.path.realpath(path)
        for member in members:
            dst = os.path.join(path, member.replace('/', os.sep))
            self.extract(member, dst)
            if not os.path.islink(dst):
                assert os.path.exists(dst)
