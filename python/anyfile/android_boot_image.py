import os
import sys
import struct

from .abstract_file import AbstractFile
from .named_location_file import NamedLocationFile

# https://android.googlesource.com/platform/system/tools/mkbootimg/+/refs/heads/main/include/bootimg/bootimg.h

BOOT_NAME_SIZE = 16
BOOT_ARGS_SIZE = 512
BOOT_EXTRA_ARGS_SIZE = 1024

VENDOR_BOOT_ARGS_SIZE = 2048
VENDOR_BOOT_NAME_SIZE = 16

class AndroidBootImageV0(NamedLocationFile):
    HEADER_SIZE = 0x30 + BOOT_NAME_SIZE + BOOT_ARGS_SIZE + 0x20 + BOOT_EXTRA_ARGS_SIZE

    def __init__(self, path, mode='r'):
        self.path = path
        self.mode = mode
        self.endian_fmt = '<'
        self.header_size = self.HEADER_SIZE
        self.magic = 'ANDROID!'
        self.kernel_size = 0
        self.kernel_addr = 0
        self.ramdisk_size = 0
        self.ramdisk_addr = 0
        self.second_size = 0
        self.second_addr = 0
        self.tags_addr = 0
        self.page_size = 0x1000
        self.header_version = 0
        self.os_version = 0
        self.name = ''
        self.cmdline = ''
        self.id = [0 for _ in range(0, 8)]
        self.extra_cmdline = ''

    def __enter__(self, *args, **kwargs):
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
        self.magic = data[0:8].decode('ascii')
        self.kernel_size = struct.unpack('{}I'.format(self.endian_fmt), data[8: 0xc])[0]
        self.kernel_addr = struct.unpack('{}I'.format(self.endian_fmt), data[0xc: 0x10])[0]
        self.ramdisk_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x10: 0x14])[0]
        self.ramdisk_addr = struct.unpack('{}I'.format(self.endian_fmt), data[0x14: 0x18])[0]
        self.second_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x18: 0x1c])[0]
        self.second_addr = struct.unpack('{}I'.format(self.endian_fmt), data[0x1c: 0x20])[0]
        self.tags_addr = struct.unpack('{}I'.format(self.endian_fmt), data[0x20: 0x24])[0]
        self.page_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x24: 0x28])[0]
        self.header_version = struct.unpack('{}I'.format(self.endian_fmt), data[0x28: 0x2c])[0]
        self.os_version = struct.unpack('{}I'.format(self.endian_fmt), data[0x2c: 0x30])[0]
        self.name = data[0x30: 0x30 + BOOT_NAME_SIZE].split(b'\x00')[0].decode('ascii')
        self.cmdline = data[0x30 + BOOT_NAME_SIZE: 0x30 + BOOT_NAME_SIZE + BOOT_ARGS_SIZE].split(b'\x00')[0].decode('ascii')
        self.id = list(struct.unpack('{}IIIIIIII'.format(self.endian_fmt), data[0x30 + BOOT_NAME_SIZE + BOOT_ARGS_SIZE: 0x30 + BOOT_NAME_SIZE + BOOT_ARGS_SIZE + 0x20]))
        assert len(self.id) == 8
        self.extra_cmdline = data[0x30 + BOOT_NAME_SIZE + BOOT_ARGS_SIZE + 0x20: 0x30 + BOOT_NAME_SIZE + BOOT_ARGS_SIZE + 0x20 + BOOT_EXTRA_ARGS_SIZE].split(b'\x00')[0].decode('ascii')
        return self

    def __exit__(self, *args, **kwargs):
        pass

    @property
    def header_offset(self):
        return 0

    @property
    def kernel_offset(self):
        return self.page_size

    @property
    def kernel_size_in_pages(self):
        return int((self.kernel_size + (self.page_size - 1)) / self.page_size)

    @property
    def ramdisk_offset(self):
        return self.kernel_offset + (self.kernel_size_in_pages * self.page_size)

    @property
    def ramdisk_size_in_pages(self):
        return int((self.ramdisk_size + (self.page_size - 1)) / self.page_size)

    @property
    def second_offset(self):
        return self.ramdisk_offset + (self.ramdisk_size_in_pages * self.page_size)

    @property
    def second_size_in_pages(self):
        return int((self.second_size + (self.page_size - 1)) / self.page_size)

    def named_locations(self):
        file_list = list()
        file_list.append(('header', self.header_offset, self.header_size))
        if self.kernel_size != 0:
            file_list.append(('kernel', self.kernel_offset, self.kernel_size))
        if self.ramdisk_size != 0:
            file_list.append(('ramdisk', self.ramdisk_offset, self.ramdisk_size))
        if self.second_size != 0:
            file_list.append(('second', self.second_offset, self.second_size))
        return file_list


class AndroidBootImageV1(AndroidBootImageV0):
    HEADER_SIZE = AndroidBootImageV0.HEADER_SIZE + 0x10

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.header_size = self.HEADER_SIZE
        self.header_version = 1
        self.recovery_dtbo_size = 0
        self.recovery_dtbo_offset = 0

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
            data = data[AndroidBootImageV0.HEADER_SIZE:]
        self.recovery_dtbo_size = struct.unpack('{}I'.format(self.endian_fmt), data[0: 0x4])[0]
        self.recovery_dtbo_offset = struct.unpack('{}Q'.format(self.endian_fmt), data[4: 0xc])[0]
        self.header_size = struct.unpack('{}I'.format(self.endian_fmt), data[0xc: 0x10])[0]
        return self

    @property
    def recovery_dtbo_size_in_pages(self):
        return int((self.recovery_dtbo_size + (self.page_size - 1)) / self.page_size)

    def __exit__(self, *args, **kwargs):
        return super().__exit__(*args, **kwargs)

    def named_locations(self):
        file_list = super().named_locations()
        if self.recovery_dtbo_size != 0:
            file_list.append(('recovery_dtbo', self.recovery_dtbo_offset, self.recovery_dtbo_size))
        return file_list


class AndroidBootImageV2(AndroidBootImageV1):
    HEADER_SIZE = AndroidBootImageV1.HEADER_SIZE + 0xc

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.header_size = self.HEADER_SIZE
        self.header_version = 2
        self.dtb_size = 0
        self.dtb_addr = 0

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
            data = data[AndroidBootImageV1.HEADER_SIZE:]
        self.dtb_size = struct.unpack('{}I'.format(self.endian_fmt), data[0: 0x4])[0]
        self.dtb_addr = struct.unpack('{}Q'.format(self.endian_fmt), data[4: 0xc])[0]
        return self

    def __exit__(self, *args, **kwargs):
        return super().__exit__(*args, **kwargs)

    @property
    def dtb_offset(self):
        return self.second_offset + ((self.second_size_in_pages + self.recovery_dtbo_size_in_pages) * self.page_size)

    @property
    def dtb_size_size_in_pages(self):
        return int((self.dtb_size + (self.page_size - 1)) / self.page_size)

    def named_locations(self):
        file_list = super().named_locations()
        if self.dtb_size != 0:
            file_list.append(('dtb', self.dtb_offset, self.dtb_size))
        return file_list


class AndroidBootImageV3(NamedLocationFile):
    HEADER_SIZE = 0x2c + BOOT_ARGS_SIZE + BOOT_EXTRA_ARGS_SIZE

    def __init__(self, path, mode='r'):
        self.path = path
        self.mode = mode
        self.endian_fmt = '<'
        self.magic = 'ANDROID!'
        self.kernel_size = 0
        self.ramdisk_size = 0
        self.os_version = 0
        self.header_size = self.HEADER_SIZE
        self.reserved = [0, 0, 0, 0]
        self.header_version = 4
        self.cmdline = ''

    def __enter__(self, *args, **kwargs):
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
        self.magic = data[0:8].decode('ascii')
        self.kernel_size = struct.unpack('{}I'.format(self.endian_fmt), data[8: 0xc])[0]
        self.ramdisk_size = struct.unpack('{}I'.format(self.endian_fmt), data[0xc: 0x10])[0]
        self.os_version = struct.unpack('{}I'.format(self.endian_fmt), data[0x10: 0x14])[0]
        self.header_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x14: 0x18])[0]
        self.reserved = list(struct.unpack('{}IIII'.format(self.endian_fmt), data[0x18: 0x28]))
        self.header_version = struct.unpack('{}I'.format(self.endian_fmt), data[0x28: 0x2c])[0]
        self.cmdline = data[0x2c: 0x2c + BOOT_ARGS_SIZE + BOOT_EXTRA_ARGS_SIZE].split(b'\x00')[0].decode()
        return self

    def __exit__(self, *args, **kwargs):
        pass

    @property
    def header_offset(self):
        return 0

    @property
    def kernel_offset(self):
        return 0x1000

    @property
    def kernel_size_in_pages(self):
        return int((self.kernel_size + (0x1000 - 1)) / 0x1000)

    @property
    def ramdisk_offset(self):
        return self.kernel_offset + (self.kernel_size_in_pages * 0x1000)

    @property
    def ramdisk_size_in_pages(self):
        return int((self.ramdisk_size + (0x1000 - 1)) / 0x1000)

    def named_locations(self):
        file_list = list()
        file_list.append(('header', self.header_offset, self.header_size))
        if self.kernel_size != 0:
            file_list.append(('kernel', self.kernel_offset, self.kernel_size))
        if self.ramdisk_size != 0:
            file_list.append(('ramdisk', self.ramdisk_offset, self.ramdisk_size))
        return file_list


class AndroidBootImageV4(AndroidBootImageV3):
    HEADER_SIZE = AndroidBootImageV3.HEADER_SIZE + 4

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.header_size = self.HEADER_SIZE
        self.header_version = 4
        self.signature_size = 0

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
            data = data[AndroidBootImageV3.HEADER_SIZE:]
        self.signature_size = struct.unpack('{}I'.format(self.endian_fmt), data[0: 4])[0]
        return self

    def __exit__(self, *args, **kwargs):
        return super().__exit__(*args, **kwargs)

    @property
    def signature_offset(self):
        return self.ramdisk_offset + (self.ramdisk_size_in_pages * 0x1000)

    @property
    def signature_size_in_pages(self):
        return int((self.signature_size + (0x1000 - 1)) / 0x1000)

    def named_locations(self):
        file_list = super().named_locations()
        if self.signature_size != 0:
            file_list.append(('signature', self.signature_offset, self.signature_size))
        return file_list


class AndroidBootImage(AbstractFile):
    HEADER_VERSION_MAP = {
        0: AndroidBootImageV0,
        1: AndroidBootImageV1,
        2: AndroidBootImageV2,
        3: AndroidBootImageV3,
        4: AndroidBootImageV4
    }

    @classmethod
    def match0(self, start):
        return start.startswith(b'ANDROID!')

    @classmethod
    def skip0(self, member):
        return member == 'header'  # stop infinite recursion on ANDRIOD! header

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.header = None

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        try:
            with open(self.path, 'rb') as handle:
                data = handle.read(0x2c)
            header_version = struct.unpack('<I', data[0x28: 0x2c])[0]
            if header_version == 0:
                page_size = struct.unpack('<I', data[0x24: 0x28])[0]
                if 0 == page_size:
                    raise NotImplementedError('Endianess detect')
                if page_size >= 0x80000:
                    endian_fmt = '>'
                else:
                    endian_fmt = '<'
            else:
                if header_version >= 0xffff:
                    endian_fmt = '>'
                else:
                    endian_fmt = '<'
            header_version = struct.unpack('{}I'.format(endian_fmt), data[0x28: 0x2c])[0]
            assert header_version < 100  # should not hit for a long time, good sanity check for now
            self.header = self.HEADER_VERSION_MAP[header_version](self.path, self.mode)
            self.header.endian_fmt = endian_fmt
            return self.header.__enter__(*args, **kwargs)
        except:
            super().__exit__(None, None, None)
            raise

    def __exit__(self, *args, **kwargs):
        try:
            return self.header.__exit__(*args, **kwargs)
        finally:
            super().__exit__(*args, **kwargs)

    def namelist(self):
        return self.header.namelist()

    def extract(self, *args, **kwargs):
        return self.header.extract(*args, **kwargs)


class VendorBootImageV3(NamedLocationFile):
    HEADER_SIZE = 0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE + 0x10

    def __init__(self, path, mode='r'):
        self.path = path
        self.mode = mode
        self.endian_fmt = '<'
        self.magic = 'VNDRBOOT'
        self.header_version = 3
        self.page_size = 0x1000
        self.kernel_addr = 0
        self.ramdisk_addr = 0
        self.vendor_ramdisk_size = 0
        self.cmdline = ''
        self.tags_addr = 0
        self.name = ''
        self.header_size = self.HEADER_SIZE
        self.dtb_size = 0
        self.dtb_addr = 0

    def __enter__(self, *args, **kwargs):
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
        self.magic = data[0:8].decode('ascii')
        self.header_version = struct.unpack('{}I'.format(self.endian_fmt), data[8: 0xc])[0]
        self.page_size = struct.unpack('{}I'.format(self.endian_fmt), data[0xc: 0x10])[0]
        self.kernel_addr = struct.unpack('{}I'.format(self.endian_fmt), data[0x10: 0x14])[0]
        self.ramdisk_addr = struct.unpack('{}I'.format(self.endian_fmt), data[0x14: 0x18])[0]
        self.vendor_ramdisk_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x18: 0x1c])[0]
        self.cmdline = data[0x1c: 0x1c + VENDOR_BOOT_ARGS_SIZE].split(b'\x00')[0].decode()
        self.tags_addr = struct.unpack('{}I'.format(self.endian_fmt), data[0x1c + VENDOR_BOOT_ARGS_SIZE: 0x1c + VENDOR_BOOT_ARGS_SIZE + 4])[0]
        self.name = data[0x1c + VENDOR_BOOT_ARGS_SIZE + 4: 0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE].split(b'\x00')[0].decode()
        self.header_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE: 0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE + 4])[0]
        self.dtb_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE + 4: 0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE + 8])[0]
        self.dtb_addr = struct.unpack('{}Q'.format(self.endian_fmt), data[0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE + 8: 0x1c + VENDOR_BOOT_ARGS_SIZE + 4 + VENDOR_BOOT_NAME_SIZE + 0x10])[0]
        return self

    def __exit__(self, *args, **kwargs):
        pass

    @property
    def header_offset(self):
        return 0

    @property
    def header_size_in_pages(self):
        return int((self.header_size + (self.page_size - 1)) / self.page_size)

    @property
    def vendor_ramdisk_offset(self):
        return self.header_offset + (self.header_size_in_pages * self.page_size)

    @property
    def vendor_ramdisk_size_in_pages(self):
        return int((self.vendor_ramdisk_size + (self.page_size - 1)) / self.page_size)

    @property
    def dtb_offset(self):
        return self.vendor_ramdisk_offset + (self.vendor_ramdisk_size_in_pages * self.page_size)

    @property
    def dtb_size_in_pages(self):
        return int((self.dtb_size + (self.page_size - 1)) / self.page_size)

    def named_locations(self):
        file_list = list()
        file_list.append(('header', self.header_offset, self.header_size))
        if self.vendor_ramdisk_size != 0:
            file_list.append(('vendor_ramdisk', self.vendor_ramdisk_offset, self.vendor_ramdisk_size))
        if self.dtb_size != 0:
            file_list.append(('dtb', self.dtb_offset, self.dtb_size))
        return file_list


class VendorBootImageV4(VendorBootImageV3):
    HEADER_SIZE = VendorBootImageV3.HEADER_SIZE # TODO: more fields

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.header_size = self.HEADER_SIZE
        self.header_version = 4
        # TODO: more fields

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        with open(self.path, 'rb') as handle:
            data = handle.read(self.HEADER_SIZE)
            data = data[AndroidBootImageV3.HEADER_SIZE:]
        # TODO: more fields
        return self

    def __exit__(self, *args, **kwargs):
        return super().__exit__(*args, **kwargs)

    def named_locations(self):
        file_list = super().named_locations()
        # TODO: more files
        return file_list


class VendorBootImage(AbstractFile):
    HEADER_VERSION_MAP = {
        3: VendorBootImageV3,
        4: VendorBootImageV4
    }

    @classmethod
    def match0(self, start):
        return start.startswith(b'VNDRBOOT')

    @classmethod
    def skip0(self, member):
        return member == 'header'  # stop infinite recursion on VNDRBOOT header

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.header = None

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        try:
            with open(self.path, 'rb') as handle:
                data = handle.read(0xc)
            header_version = struct.unpack('<I', data[0x8: 0xc])[0]
            if header_version == 0:
                endian_fmt = None
            else:
                if header_version >= 0xffff:
                    endian_fmt = '>'
                else:
                    endian_fmt = '<'
            header_version = struct.unpack('{}I'.format(endian_fmt), data[0x8: 0xc])[0]
            assert header_version < 100  # should not hit for a long time, good sanity check for now
            self.header = self.HEADER_VERSION_MAP[header_version](self.path, self.mode)
            self.header.endian_fmt = endian_fmt
            return self.header.__enter__(*args, **kwargs)
        except:
            super().__exit__(None, None, None)
            raise

    def __exit__(self, *args, **kwargs):
        try:
            return self.header.__exit__(*args, **kwargs)
        finally:
            super().__exit__(*args, **kwargs)

    def namelist(self):
        return self.header.namelist()

    def extract(self, *args, **kwargs):
        return self.header.extract(*args, **kwargs)
