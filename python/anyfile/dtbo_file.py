import os
import sys
import struct

from .named_location_file import NamedLocationFile


class DtboFile(NamedLocationFile):
    HEADER_SIZE = 0x20
    ENTRY_SIZE = 0x20

    @classmethod
    def match0(self, start):
        return start.startswith(b'\xd7\xb7\xab\x1e')

    @classmethod
    def skip0(self, member):
        return member == 'header'  # stop infinite recursion on header

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.endian_fmt = '>'
        self.magic = b'\xd7\xb7\xab\x1e'
        self.total_size = 0
        self.header_size = self.HEADER_SIZE
        self.entry_size = self.ENTRY_SIZE
        self.entry_count = 0
        self.first_entry_offset = self.header_size
        self.page_size = 0x1000
        self.image_version = 0
        self.dtb_size = list()
        self.dtb_offset = list()
        self.dtb_device_id = list()
        self.dtb_device_revision = list()
        self.dtb_unused = list()

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        try:
            with open(self.path, 'rb') as handle:
                data = handle.read(self.HEADER_SIZE)
            self.magic = data[0: 4]
            self.header_size = struct.unpack('<I', data[8: 0xc])[0]
            if self.header_size > 0xffff:
                self.endian_fmt = '>'
            else:
                self.endian_fmt = '<'
            self.total_size = struct.unpack('{}I'.format(self.endian_fmt), data[4: 0x8])[0]
            self.header_size = struct.unpack('{}I'.format(self.endian_fmt), data[8: 0xc])[0]
            self.entry_size = struct.unpack('{}I'.format(self.endian_fmt), data[0xc: 0x10])[0]
            self.entry_count = struct.unpack('{}I'.format(self.endian_fmt), data[0x10: 0x14])[0]
            self.first_entry_offset = struct.unpack('{}I'.format(self.endian_fmt), data[0x14: 0x18])[0]
            self.page_size = struct.unpack('{}I'.format(self.endian_fmt), data[0x18: 0x1c])[0]
            self.image_version = struct.unpack('{}I'.format(self.endian_fmt), data[0x1c: 0x20])[0]
            with open(self.path, 'rb') as handle:
                data = handle.read(self.header_and_entry_size)
            self.dtb_size = [0 for _ in range(0, self.entry_count)]
            self.dtb_offset = [0 for _ in range(0, self.entry_count)]
            self.dtb_device_id = [0 for _ in range(0, self.entry_count)]
            self.dtb_device_revision = [0 for _ in range(0, self.entry_count)]
            self.dtb_unused = [(b'\x00' * 0x10) for _ in range(0, self.entry_count)]
            offset = self.first_entry_offset
            for i in range(0, self.entry_count):
                self.dtb_size[i] = struct.unpack('{}I'.format(self.endian_fmt), data[offset + 0: offset + 4])[0]
                self.dtb_offset[i] = struct.unpack('{}I'.format(self.endian_fmt), data[offset + 4: offset + 8])[0]
                self.dtb_device_id[i] = struct.unpack('{}I'.format(self.endian_fmt), data[offset + 8: offset + 0xc])[0]
                self.dtb_device_revision[i] = struct.unpack('{}I'.format(self.endian_fmt), data[offset + 0xc: offset + 0x10])[0]
                self.dtb_unused[i] = data[offset + 0x10: offset + 0x20]
                offset += self.entry_size
            return self
        except:
            super().__exit__(None, None, None)
            raise

    @property
    def header_offset(self):
        return 0

    @property
    def header_and_entry_size(self):
        return self.header_size + (self.entry_size * self.entry_count)

    def named_locations(self):
        file_list = list()
        file_list.append(('header', self.header_offset, self.header_and_entry_size))
        for i in range(0, self.entry_count):
            file_list.append(('dtb{}'.format(i), self.dtb_offset[i], self.dtb_size[i]))
        return file_list
