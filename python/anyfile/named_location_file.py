import os
import sys
import subprocess

from .abstract_file import AbstractFile


class NamedLocationFile(AbstractFile):

    def namelist(self):
        return [name for name, _, _ in self.named_locations()]

    def extract(self, member, path=None, pwd=None):
        if member not in self.namelist():
            raise FileNotFoundError(member)
        src = os.path.realpath(self.path)
        if not path:
            dst = os.path.realpath(os.path.join(os.getcwd(), os.path.basename(member.replace('/', os.sep))))
        dst = os.path.realpath(path)
        dst_dir = os.path.dirname(dst)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        if os.path.exists(dst):
            os.remove(dst)
        assert os.path.exists(src)
        assert os.path.exists(dst_dir)
        assert not os.path.exists(dst)
        found = False
        offset = None
        size = None
        for name, offset, size in self.named_locations():
            if name == member:
                found = True
                break
        if not found:
            raise FileNotFoundError(member)
        args = ['dd', 'if={}'.format(src), 'of={}'.format(dst), 'bs=1M', 'skip={}'.format(offset), 'count={}'.format(size), 'iflag=skip_bytes,count_bytes']
        p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if 0 != p.returncode:
            sys.stdout.write(stdout.decode())
            sys.stderr.write(stderr.decode())
            raise Exception(' '.join(args))
        assert os.path.exists(dst)
