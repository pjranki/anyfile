import os
import sys
import string
import subprocess

from .tmp_directory import TmpDirectory


class SuperImgFile(TmpDirectory):

    @classmethod
    def match0(self, start):
        if start.startswith(b'\x67\x44\x6c\x61') or start.startswith(b'\x30\x50\x4c\x41'):
            return True
        return False

    @classmethod
    def match2(self, path):
        if os.path.basename(path).lower().find('super') == -1:
            return False
        with open(path, 'rb') as handle:
            handle.seek(0x1000, 0)
            magic = handle.read(4)
            if magic.startswith(b'\x67\x44\x6c\x61') or magic.startswith(b'\x30\x50\x4c\x41'):
                return True
        return False

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        try:
            extracted_path = os.path.join(self.tmp, 'extracted')
            assert not os.path.exists(extracted_path)
            args = ['imjtool', os.path.realpath(self.path), 'extract']
            p = subprocess.Popen(args, cwd=self.tmp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            if 0 != p.returncode:
                sys.stdout.write(stdout.decode())
                sys.stderr.write(stderr.decode())
                raise Exception(' '.join(args))
            assert os.path.exists(extracted_path)
            for name in os.listdir(extracted_path):
                args = ['mv', '{}'.format(os.path.join(extracted_path, name)), '.']
                p = subprocess.Popen(args, cwd=self.tmp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout, stderr = p.communicate()
                if 0 != p.returncode:
                    sys.stdout.write(stdout.decode())
                    sys.stderr.write(stderr.decode())
                    raise Exception(' '.join(args))
            args = ['rmdir', '{}'.format(extracted_path)]
            p = subprocess.Popen(args, cwd=self.tmp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            if 0 != p.returncode:
                sys.stdout.write(stdout.decode())
                sys.stderr.write(stderr.decode())
                raise Exception(' '.join(args))
            assert not os.path.exists(extracted_path)
            return self
        except:
            super().__exit__(None, None, None)
            raise
