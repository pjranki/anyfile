import os
import sys
import string
import zipfile
import subprocess

from .tmp_directory import TmpDirectory


class ZipFile(TmpDirectory):

    @classmethod
    def match0(self, start):
        return start.startswith(b'PK\x03\x04')

    @property
    def encrypted(self):
        args = ['7z', 'l', '-slt', os.path.realpath(self.path)]
        p = subprocess.Popen(args, cwd=self.tmp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if 0 != p.returncode:
            sys.stdout.write(stdout.decode())
            sys.stderr.write(stderr.decode())
            raise Exception(' '.join(args))
        assert stdout.decode().find('7-Zip') != -1
        return stdout.decode().find('ZipCrypto') != -1

    def namelist(self):
        if self.encrypted:
            return  # TODO: decrypt
        with zipfile.ZipFile(self.path, mode='r', allowZip64=True) as zip:
            # list without extraction (faster)
            return zip.namelist()

    def member_to_path(self, member):
        path = super().member_to_path(member)
        if not os.path.exists(path):
            # unzip files as needed (faster)
            args = ['unzip', os.path.realpath(self.path), member]
            p = subprocess.Popen(args, cwd=self.tmp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            if 0 != p.returncode:
                sys.stdout.write(stdout.decode())
                sys.stderr.write(stderr.decode())
                raise Exception(' '.join(args))
            if not os.path.islink(path):
                assert os.path.exists(path)
        return path
