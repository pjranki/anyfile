import os
import sys
import string
import tarfile
import subprocess

from .tmp_directory import TmpDirectory


class TarFile(TmpDirectory):

    @classmethod
    def match0(self, start):
        return start[0x100: 0x108] == b'\x00ustar\x20\x20'

    def namelist(self):
        with tarfile.TarFile(self.path, mode='r') as tar:
            # list without extraction (faster)
            return tar.getnames()

    def member_to_path(self, member):
        path = super().member_to_path(member)
        if not os.path.exists(path):
            # extract files as needed (faster)
            args = ['tar', '-xf', os.path.realpath(self.path), member]
            p = subprocess.Popen(args, cwd=self.tmp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            if 0 != p.returncode:
                sys.stdout.write(stdout.decode())
                sys.stderr.write(stderr.decode())
                raise Exception(' '.join(args))
            if not os.path.islink(path):
                assert os.path.exists(path)
        return path
