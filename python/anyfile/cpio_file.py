import os
import sys
import string
import subprocess

from .tmp_directory import TmpDirectory


class CpioFile(TmpDirectory):

    @classmethod
    def match1(self, start, end):
        if end.find(b'TRAILER!!!\x00') != -1:
            valid_bytes = b'0123456789abcdefABCDEF'
            valid = True
            for i in range(0, 16):
                byte = start[i: i + 1]
                if valid_bytes.find(byte) == -1:
                    valid = False
                    break
            if not valid:
                return False
            return True
        return False

    @property
    def fakeroot(self):
        if not self.tmp:
            return None
        return self.tmp + '.fakeroot'

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        try:
            cmdline = 'cat {} | fakeroot -s {} cpio -i'.format(os.path.realpath(self.path), self.fakeroot)
            p = subprocess.Popen(cmdline, shell=True, cwd=self.tmp, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()
            if 0 != p.returncode:
                sys.stdout.write(stdout.decode())
                sys.stderr.write(stderr.decode())
                raise Exception(cmdline)
        except:
            try:
                if self.fakeroot and os.path.exists(self.fakeroot):
                    os.remove(self.fakeroot)
            finally:
                super().__exit__(None, None, None)
            raise
        return self

    def __exit__(self, *args, **kwargs):
        try:
            if self.fakeroot and os.path.exists(self.fakeroot):
                os.remove(self.fakeroot)
                assert not os.path.exists(self.fakeroot)
            return
        finally:
            super().__exit__(*args, **kwargs)
