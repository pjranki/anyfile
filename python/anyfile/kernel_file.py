import os
import sys
import zlib
import struct
import subprocess

from .named_location_file import NamedLocationFile
from .gzip_file import GZipFile
from .tmp_directory import TmpDirectory


class KernelFile(NamedLocationFile):

    @classmethod
    def match2(self, path):
        file_name = os.path.basename(path)
        kernel_names = ['kernel']
        found = False
        for kernel_name in kernel_names:
            if file_name.lower().find(kernel_name.lower()) != -1:
                found = True
                break
        if not found:
            return False
        args = ['file', path]
        p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if 0 != p.returncode:
            sys.stdout.write(stdout.decode())
            sys.stderr.write(stderr.decode())
            raise Exception(' '.join(args))
        file_type = ':'.join(stdout.decode().split('\n')[0].split(':')[1:]).strip()
        found = False
        if file_type.lower().find('Linux kernel'.lower()) != -1:
            if file_type.lower().find('boot executable Image'.lower()) != -1:
                found = True
        if not found:
            return False
        if os.path.getsize(path) < (128 * 1024 * 1024):
            return True
        return False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.version = ''
        self.version_offset = 0
        self.version_size = 0
        self.config_name = ''
        self.config_offset = 0
        self.config_size = 0

    def __enter__(self, *args, **kwargs):
        super().__enter__(*args, **kwargs)
        try:
            with open(self.path, 'rb') as handle:
                data = handle.read()

            # exact version
            self.version = ''
            offset = 0
            while True:
                new_offset = data[offset:].find(b"Linux version")
                if new_offset < 0:
                    break
                offset = offset + new_offset
                end = offset + data[offset:].find(b'\x00')
                if end < 0:
                    end = len(data)
                text = data[offset: end].decode().strip()
                if len(text) > len(self.version):
                    self.version = text
                    self.version_offset = offset
                    self.version_size = end - offset
                offset = end

            # extract config
            self.config_name = ''
            self.config_offset = 0
            self.config_size = 0
            offset = 0
            while True:
                new_offset = data[offset:].find(b'\x1f\x8b\x08\x00')
                if new_offset < 0:
                    break
                offset = offset + new_offset
                found_config_file = False
                with TmpDirectory() as tmp:
                    config_gz_path = os.path.join(tmp.tmp, 'config.gz')
                    config_path = os.path.join(tmp.tmp, 'config')
                    with open(config_gz_path, 'wb') as handle:
                        handle.write(data[offset: len(data)])
                    with GZipFile(config_gz_path, 'r') as gzip:
                        gzip.extract(gzip.namelist()[0], config_path)
                    with open(config_path, 'rt') as handle:
                        text = handle.read()
                    uncompressed_size = os.path.getsize(config_path)
                    if (text.startswith('#') or text.startswith('CONFIG_')) and text.count('CONFIG_') > 50:
                        found_config_file = True
                if found_config_file:
                    self.config_name = 'config.gz'
                    self.config_offset = offset
                    self.config_size = uncompressed_size  # HACK: hard to determine the size of the gzip
                offset = offset + 4
            return self
        except:
            super().__exit__(None, None, None)
            raise

    def named_locations(self):
        file_list = list()
        file_list.append(('proc/version', self.version_offset, self.version_size))
        if self.config_size != 0:
            file_list.append(('proc/{}'.format(self.config_name), self.config_offset, self.config_size))
        return file_list
