import os
import sys
import subprocess

from .abstract_file import AbstractFile


class DtbFile(AbstractFile):

    @classmethod
    def match0(self, start):
        return start.startswith(b'\xd0\x0d\xfe\xed')

    def namelist(self):
        name = os.path.basename(self.path)
        if name.endswith('.dtb'):
            name = name[:-4]
        if not name.endswith('.dts'):
            name = name + '.dts'
        return [name]

    def extract(self, member, path=None, pwd=None):
        if member not in self.namelist():
            raise FileNotFoundError(member)
        src = os.path.realpath(self.path)
        if not path:
            dst = os.path.realpath(os.path.join(os.getcwd(), os.path.basename(member.replace('/', os.sep))))
        dst = os.path.realpath(path)
        dst_dir = os.path.dirname(dst)
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)
        if os.path.exists(dst):
            os.remove(dst)
        assert os.path.exists(src)
        assert os.path.exists(dst_dir)
        assert not os.path.exists(dst)
        args = ['dtc', '-I', 'dtb', '-O', 'dts', '-o', dst, src]
        p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if 0 != p.returncode:
            sys.stdout.write(stdout.decode())
            sys.stderr.write(stderr.decode())
            raise Exception(' '.join(args))
        assert os.path.exists(dst)
