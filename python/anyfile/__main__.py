import os
import sys
import argparse
from .any_file import AnyFile

import logging
import sys

log = logging.getLogger()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--in-file', type=str, required=True)
    parser.add_argument('--list', action='store_true', default=False)
    parser.add_argument('--extractall', type=str, default=None)
    parser.add_argument('--verbose', action='store_true', default=False)
    args = parser.parse_args()

    if args.verbose:
        log.setLevel(logging.DEBUG)
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        log.addHandler(handler)

    with AnyFile(args.in_file) as img:
        if args.extractall:
            img.extractall(os.path.realpath(args.extractall))
        if args.list:
            for name in img.namelist():
                print(name)


if __name__ == '__main__':
    main()
