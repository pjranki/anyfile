import os
import sys
import subprocess

PYTHONPATH = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'python'))
sys.path.insert(0, PYTHONPATH)
import anyfile
anyfile.main()
